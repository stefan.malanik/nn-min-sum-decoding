# Project Title & Author

A Hardware Implementation of a Belief Propagation Error Correction Algorithm using Neural Networks

Malanik Ștefan
	
	
# Brief Structure Description
	
The code is structured into three main folders, with an extra folder holding persistent data.
The three folders are: 
- `model/` -> Python code implementing the high-level model
- `hardware/` -> Verilog implementation of the decoder
- `testbench/` -> two subfolders, one for the C implementation of the decoder, 
				   and the other for the SystemVerilog testbench


# Running
	
## Python (`model/`)
		
For running the Python model, Jupyter Notebook with the JupyText extension is required in order
to open the two notebooks saved in `.py` format. An anaconda environment is heavily encouraged, and
the library imports are provided in the file `requirements.txt`.

* `model_notebook.py` contains all functions that are needed to train and export all data associated to
a decoder, which is required for the hardware implemetation. 
* `stats_notebook.py` uses the data generated and creates multiple scenarios for analyzing the model and generating suggestive plots.

## ModelSim (`testbench/sim/`)
		
ModelSim needs to be installed in order to run the testbench. A TCL script functions.tcl may be 
loaded into the ModelSim console with the command `'source functions.tcl'`. Then, a `'simcln'` can be
peformed, which initializes the whole project, runs code generation, and compiles the C library and 
Verilog files.

## Xilinx Vivado (`hardware/`)
		
Vivado is the tool used for hardware synthesis after the testbench verification is successful.
The source code files that are generated either have to exist already from a ModelSim project
initialization with `'simcln'`, or manually run with `generate.bat`.

The Vivado project needs to be created and all files in `hardware/` manually inserted. 
`decoder_top.sv` is the top-level module. The normal synthesis flow can be performed afterwards.

## C (`testbench/c`)

A makefile is provided to run the standalone C model for statistics. A `'make'` command builds the 
project, and `'make runc'` is a shorthand command for running the statistics with the required 
command-line arguments. The C code was built and run in a Linux environment.


